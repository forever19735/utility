//
//  ConfigCell.swift
//  Utility
//
//  Created by 季紅 on 2023/3/25.
//

import Foundation

protocol ConfigCell {
    associatedtype ViewData
    func configure(viewData: ViewData)
}

extension ConfigCell {
    func configure(viewData: ViewData) {}
}
