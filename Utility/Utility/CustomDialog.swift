//
//  CustomDialog.swift
//  Utility
//
//  Created by 季紅 on 2023/3/22.
//

import UIKit

protocol CustomDialog where Self: UIViewController {

    /// 用來持有 alertWindow，創造一個臨時的 retain cycle
    /// 不需要實作，放上 var alertWindow: UIWindow? 即可
    var alertWindow: UIWindow? { get set }

    var backgroundStyle: UIColor { get }

    func present()
}

extension CustomDialog {
    /// 使用另一個 UIWindow 去顯示 Dialog, 不會影響主視窗畫面
    func present() {
        let alertWindow = UIWindow(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)))
        // 取得 view 所屬的 windowScene，並指派給 alertWindow。
        guard let windowScene = UIApplication.shared.connectedScenes
            .first(where: { $0.activationState == .foregroundActive }) as? UIWindowScene else { return }
        alertWindow.windowScene = windowScene

        // 設定並顯示 alertWindow。
        alertWindow.backgroundColor = nil
        alertWindow.windowLevel = .alert
        alertWindow.rootViewController = UIViewController()
        alertWindow.isHidden = false

        // 用 vc 去持有 alertWindow，創造一個臨時的 retain cycle。
        // 在 vc 被釋放後，這些閉包也會被釋放，跟著把 alertWindow 給釋放掉。
        self.alertWindow = alertWindow

        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)

        // 在 alertWindow 中呈現 vc
        alertWindow.rootViewController?.present(self, animated: true)
    }
}
