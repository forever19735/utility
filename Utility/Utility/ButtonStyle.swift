//
//  ButtonStyle.swift
//  Utility
//
//  Created by 季紅 on 2023/3/21.
//

import UIKit

struct ButtonStyle {
    let backgroundColor: UIColor
    let textColor: UIColor
    let cornerRadius: CGFloat
    let font: UIFont

    init(backgroundColor: UIColor, textColor: UIColor, cornerRadius: CGFloat, font: UIFont) {
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.cornerRadius = cornerRadius
        self.font = font
    }

    static var `default`: ButtonStyle {
        return ButtonStyle(backgroundColor: UIColor(hex: "#191919"),
                           textColor: UIColor.white,
                           cornerRadius: 8.0,
                           font: FontBook.font(.bold, fontSize: .normal))
    }

    static var disable: ButtonStyle {
        return ButtonStyle(backgroundColor: UIColor(hex: "#767676"),
                           textColor: UIColor.white,
                           cornerRadius: 8.0,
                           font: FontBook.font(.bold, fontSize: .normal))
    }
}

