//
//  UIButton + Extensions.swift
//  Utility
//
//  Created by 季紅 on 2023/3/21.
//

import UIKit

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        setBackgroundImage(colorImage, for: forState)
    }

    func apply(style: ButtonStyle, state: UIControl.State = .normal) {
        titleLabel?.font = style.font
        setBackgroundColor(color: style.backgroundColor, forState: state)
        setTitleColor(style.textColor, for: state)
        layer.cornerRadius = style.cornerRadius
        layer.masksToBounds = true
    }
}
